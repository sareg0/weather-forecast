import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import Chart from './Chart.js';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }

    let id = setInterval(tick, delay);
    return () => clearInterval(id);
  }, [delay]);
}

function App() {
  const [data, setChartData] = useState(null);

  useInterval(fetchData, 60000);

  async function fetchData () {
    const result = await axios(`https://api.openweathermap.org/data/2.5/forecast?lon=-122.403370&lat=37.791130&appid=${process.env.REACT_APP_API_TOKEN}&units=metric`)
    setChartData(result.data.list)
  }

  useEffect(() => {
    fetchData()
  }, [])

  if (!data) {
    return <h3>Loading...</h3>
  } else {
    return <Chart data={data} />
  }
}

export default App;
