import React from 'react';
import './Chart.css';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Legend, Tooltip } from 'recharts';
var format = require('date-fns/format')

function Chart(props) {
  const data = props.data

  const convertedTimeStamp = (unix_ts_seconds) => {
    // the timestamp from API is seconds
    // we need to multiple by 1000
    return new Date(unix_ts_seconds * 1000)
  }

  return (
    <section className="Chart">
      <h1>5-day forecast for GitLab's Only Address</h1>
      <LineChart width={600} height={300} data={data}>
        <CartesianGrid stroke="#ccc" strokeDasharray="3 3" />
        <XAxis name="date" dataKey="dt" tickFormatter={(sec) => format(convertedTimeStamp(sec), 'ha dd') } />
        <YAxis dataKey="main.temp" />
        <Legend />
        <Tooltip 
            formatter={ (value) => [`${value}°C`, "temp"] }
            labelFormatter={ (sec) => format(convertedTimeStamp(sec), 'ha, Do MMM') } />
        <Line name="temperature" type="monotone" dataKey="main.temp" stroke='#6b4fbb' strokeWidth={2} />
      </LineChart>
    </section>
  );
}

export default Chart;
